from invoke import task
import os

REPO_URL = 'git@bitbucket.org:nate_vogel/superlists.git'
HOST = 'superlists.com' #TODO. Should not be hard coded.

@task
def deploy():
    site_folder = \
    '/home/{user}/sites/{host}'.format(user=os.environ['LOGNAME'],host=HOST)
    source_folder = site_folder + '/source'
    pass
